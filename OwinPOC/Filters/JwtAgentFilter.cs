﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Http.Results;

namespace OwinPOC.Filters
{
    public class JwtAgentFilter : ActionFilterAttribute, IAuthenticationFilter
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
        }

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var descriptor = (context.ActionContext.ActionDescriptor);
            var attrib = descriptor.GetCustomAttributes<AuthorizeAttribute>()
                .FirstOrDefault();
            if (attrib == null)
            {
                context.ErrorResult = new UnauthorizedResult(new []
                {
                    new AuthenticationHeaderValue("bearer"), 
                }, context.Request);
                return Task.CompletedTask;
            }
            var agent = context.Request.Headers.UserAgent;
            var agentValue = agent.ToString();
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var claim = identity.Claims.FirstOrDefault(p => p.Type == "agent");
                if (claim == null)
                {
                    context.ErrorResult = new UnauthorizedResult(new []
                    {
                        new AuthenticationHeaderValue("bearer"), 
                    }, context.Request);
                    return Task.CompletedTask;
                    
                }
                byte[] data = Convert.FromBase64String(claim.Value);
                string decodedString = Encoding.UTF8.GetString(data);
                if (decodedString == agentValue)
                {
                    context.ErrorResult = null;
                    return Task.CompletedTask;
                }
            }
            context.ErrorResult = new UnauthorizedResult(new []
            {
                new AuthenticationHeaderValue("bearer"), 
            }, context.Request);
            return Task.CompletedTask;
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}