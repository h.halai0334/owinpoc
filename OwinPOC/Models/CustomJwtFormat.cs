﻿using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;

namespace OwinPOC.Models
{
    public static class JwtOptions
    {
        public static string Issuer = "http://localhost:5000";
        public static string Secret = "IxrAjDoa2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw";
        public static string Audience = "AudienceName";

    }
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const string AudiencePropertyKey = "audience";
 
        private readonly string _issuer = JwtOptions.Issuer;
 
        public CustomJwtFormat()
        {
        }
 
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            var keyByteArray = TextEncodings.Base64Url.Decode(JwtOptions.Secret);
 
            var signingKey = new SigningCredentials(new SymmetricSecurityKey(keyByteArray),"HS256");
 
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;
 
            var token = new JwtSecurityToken(_issuer, JwtOptions.Audience, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);
 
            var handler = new JwtSecurityTokenHandler();
 
            var jwt = handler.WriteToken(token);
 
            return jwt;
        }
 
        public AuthenticationTicket Unprotect(string protectedText)
        {
            return null;
        }
    }
}