﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OwinPOC.Context;
using OwinPOC.Filters;
using OwinPOC.Models;

namespace OwinPOC.Controllers
{
    [JwtAgentFilter]
    public class TestController : ApiController
    {
        public TestController()
        {
        
        }

        [HttpGet]
        [Authorize]
        public string TestMethod()
        {
            return "Authenticated";
        }

        [HttpPost]
        public async Task<IHttpActionResult> Register([FromBody] RegisterRequestModel model)
        {
            var context = new OwinAuthDbContext();
            var roles = context.Roles.ToList();
            IdentityRole role = roles.FirstOrDefault(p => p.Name == "Customer");
            if (role == null)
            {
                role = new IdentityRole()
                {
                    Name = "Customer"
                };
                context.Roles.Add(role);
                await context.SaveChangesAsync();
            }

            var user = new IdentityUser()
            {
                Email = model.Password,
                UserName = model.Username,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            var hasher=  new PasswordHasher();
            user.PasswordHash = hasher.HashPassword(model.Password);
            context.Users.Add(user);
            await context.SaveChangesAsync();
            return Ok(new
            {
                success = true,
                message = "User Created"
            });
        }
    }  
}