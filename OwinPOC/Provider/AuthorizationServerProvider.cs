﻿using System;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OAuth;

namespace OwinPOC.Provider
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserManager<IdentityUser> userManager = context.OwinContext.GetUserManager<UserManager<IdentityUser>>();  
            IdentityUser user;  
            try  
            {  
                user = await userManager.FindAsync(context.UserName, context.Password);  
            }  
            catch  
            {  
                // Could not retrieve the user due to error.  
                context.SetError("server_error");  
                context.Rejected();  
                return;  
            }  
            if (user != null)  
            {  
                ClaimsIdentity identity = await userManager.CreateIdentityAsync(  
                    user,  
                    DefaultAuthenticationTypes.ExternalBearer);
                identity.AddClaim(new Claim("custom", "customData"));
                var stringData = Encoding.UTF8.GetBytes(context.Request.Headers["User-Agent"]);
                var encryptedBase64 = Convert.ToBase64String(stringData);
                identity.AddClaim(new Claim("agent", encryptedBase64));
                context.Validated(identity);  
            }  
            else  
            {  
                context.SetError("invalid_grant", "Invalid User Id or password'");  
                context.Rejected();  
            }  
        }
    }
}