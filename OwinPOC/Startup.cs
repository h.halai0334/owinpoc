﻿using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;
using OwinPOC.Context;
using OwinPOC.Models;
using OwinPOC.Provider;


[assembly: OwinStartup(typeof(OwinPOC.Startup))]  
namespace OwinPOC
{
    public class Startup  
    {  
        public void Configuration(IAppBuilder app)  
        {  
            ConfigureOAuth(app);
        }  
        
        private void ConfigureOAuth(IAppBuilder app)  
        {  
            app.CreatePerOwinContext(() => new OwinAuthDbContext());  
            app.CreatePerOwinContext<UserManager<IdentityUser>>(CreateManager);  
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions  
            {  
                TokenEndpointPath = new PathString("/oauth/token"),  
                Provider = new AuthorizationServerProvider(),  
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),  
                AllowInsecureHttp = true,  
                AccessTokenFormat = new CustomJwtFormat(),
            });
            var keyByteArray = Microsoft.Owin.Security.DataHandler.Encoder.TextEncodings.Base64Url.Decode(JwtOptions.Secret);
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new []{ JwtOptions.Audience },
                    IssuerSecurityKeyProviders = new IIssuerSecurityKeyProvider[]
                    {
                        new SymmetricKeyIssuerSecurityKeyProvider(JwtOptions.Issuer, keyByteArray), 
                    },
                });
        }  
  
        private static UserManager<IdentityUser> CreateManager (IdentityFactoryOptions<UserManager<IdentityUser>> options, IOwinContext context)  
        {  
            var userStore = new UserStore<IdentityUser>(context.Get<OwinAuthDbContext>());  
            var owinManager = new UserManager<IdentityUser>(userStore);  
            return owinManager;  
        }  
    }  
}