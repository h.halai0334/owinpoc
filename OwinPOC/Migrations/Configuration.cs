using Microsoft.AspNet.Identity;

namespace OwinPOC.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Context.OwinAuthDbContext>
    {
       
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context.OwinAuthDbContext context)
        {
        
        }
    }
}
