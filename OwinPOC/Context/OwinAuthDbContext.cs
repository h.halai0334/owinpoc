﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace OwinPOC.Context
{
    public class OwinAuthDbContext : IdentityDbContext<IdentityUser,IdentityRole,string,IdentityUserLogin,IdentityUserRole,IdentityUserClaim>
    {  
        public OwinAuthDbContext()  
            : base("name=OwinAuthDbContext")
        {
            if (!Database.Exists())
            {
                Database.Create();
            }
        }  
    }  
}